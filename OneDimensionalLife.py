import Rhino.Geometry as rg
import System.Drawing.Color as col

def Pixeldraw(Cen,Plane,U,V):
    PixMesh=rg.Mesh()
    PixelsList=[]
    N00=Cen-Plane.XAxis*(U/2)-Plane.YAxis*(V/2)
    N10=N00+Plane.XAxis*U
    N11=N10+Plane.YAxis*V
    N01=N00+Plane.YAxis*V
    PixMesh.Vertices.Add(N00)
    PixMesh.Vertices.Add(N10)
    PixMesh.Vertices.Add(N11)
    PixMesh.Vertices.Add(N01)
    PixMesh.Faces.AddFace(0,1,2,3)
    return PixMesh


# here we collect centers with their i&j counters which are inside the curve
#Tuples={}
#Pixels={}
#for i in range(0,m):
#    for j in range(0,n):
#        center=Pl.Origin+Pl.XAxis*i*Usize+Pl.YAxis*j*Vsize
#        #if (crv.Contains(center,Pl,tol)==rg.PointContainment.Inside):
#            #Tuple.setdefault((i,j),center)
#        Pix=Pixeldraw(center,Pl,Usize,Vsize)
#        Pixels.setdefault((i,j),Pix)
#a=PixelsList

def ca(m,n):
   ''' Celluar automata with Python - K. Hong'''
   # 64 Boolean - True(1) : '*'
   #            - False(0): '-'
   # Rule - the status of current cell value is True
   # if only one of the two neighbors at the previous step is True('*')
   # otherwise, the current cell status is False('-')

   # list representing the current status of 64 cells
   ca=[]
   for i in range(m):
       ca.append(0)
   ca[int(m/2)]=1
   # new Cellular values
   ca_new = ca[:]
   #Pixels={}
   PixelsList=[]
   
   # dictionary maps the cell value to a symbol
   dic = {0:col.Black, 1:col.White}
    
   # initial draw - step 0
   #print(''.join( [dic[e] for e in ca_new]))
   for i in range(0,m):
       center=Pl.Origin+Pl.XAxis*i*Usize#+Pl.YAxis*j*Vsize
       Pix=Pixeldraw(center,Pl,Usize,Vsize)
       Pix.VertexColors.CreateMonotoneMesh(dic[ca_new[i]])
   # additional 31 steps
       PixelsList.append(Pix)
   step = 1
   while(step < n):
      ca_new = []
      # loop through 0 to 63 and store the current cell status in ca_new list
      for i in range(0,m):
         # inside cells - check the neighbor cell state
         if i > 0 and i < (m-1):
            if ca[i-1] == ca[i+1]:
               ca_new.append(0)
            else:
               ca_new.append(1)

         # left-most cell : check the second cell
         elif(i == 0):
            if ca[1] == 1:
               ca_new.append(1)
            else:
               ca_new.append(0)

         # right-most cell : check the second to the last cell
         elif(i == (m-1)):
            if ca[m-2] == 1:
               ca_new.append(1)
            else:
               ca_new.append(0)

      # draw current cell state
      #print(''.join( [dic[e] for e in ca_new]))
      for i in range(0,m):
         center=Pl.Origin+Pl.XAxis*i*Usize-Pl.YAxis*step*Vsize
         Pix=Pixeldraw(center,Pl,Usize,Vsize)
         Pix.VertexColors.CreateMonotoneMesh(dic[ca_new[i]])
         PixelsList.append(Pix)
      # update cell list
      ca = ca_new[:]

      # step count
      step += 1
   return PixelsList
b=ca(m,n)
#if __name__ == '__main__':
#   ca(m,n)