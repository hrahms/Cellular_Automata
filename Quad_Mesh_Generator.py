import Rhino.Geometry as rg
import System.Drawing.Color as col

def Pixeldrawing(Cen,Plane,U,V):
    PixMesh=rg.Mesh()
    PixelsList=[]
    N00=Cen-Plane.XAxis*(U/2)-Plane.YAxis*(V/2)
    N10=N00+Plane.XAxis*U
    N11=N10+Plane.YAxis*V
    N01=N00+Plane.YAxis*V
    PixMesh.Vertices.Add(N00)
    PixMesh.Vertices.Add(N10)
    PixMesh.Vertices.Add(N11)
    PixMesh.Vertices.Add(N01)
    PixMesh.Faces.AddFace(0,1,2,3)
    return PixMesh

def CombineSurfaces(Pl,m,n,Usize,Vsize):
    surfacemesh=rg.Mesh()
    for i in range(0,m):
        for j in range(0,n):
            center=Pl.Origin+Pl.XAxis*i*Usize+Pl.YAxis*j*Vsize
            Pixel=Pixeldrawing(center,Pl,Usize,Vsize)
            surfacemesh.Append(Pixel)
    surfacemesh.Vertices.CullUnused
    surfacemesh.Vertices.CombineIdentical(False, False)
    return surfacemesh




M=CombineSurfaces(Pl,m,n,Usize,Vsize)